FROM openjdk:16-jdk-alpine
WORKDIR /app
ARG H2
ARG JAR
ENV env_h2=$H2
ENV env_jar=$JAR
COPY ./target/*.jar /app
#ENV JAR=assignment-0.0.1-SNAPSHOT-210818-09bf92fc.jar
#ENV H2=-Dspring.profiles.active=h2
CMD java -jar $env_h2 $env_jar
