#!/usr/bin/bash

image="assignmet"
commHash=`git log -1 --pretty=%h`
dateBuild=`date +%y%m%d`

t1=$dateBuild-$commHash
tag=$image:$t1

#jar=assignment-0.0.1-SNAPSHOT-210818-09bf92fc.jar
#h2=-Dspring.profiles.active=h2
#H2="-Dspring.profiles.active=h2"

h2=""
jar="assignment-0.0.1-SNAPSHOT-210818-09bf92fc.jar"

docker build -t $tag --build-arg H2=${h2} --build-arg JAR=${jar} .
#docker tag $tag eyadzidan/$image:$t1
#docker login
#docker push eyadzidan/assignmet:$t1
